# © 2019 Paul John King (paul_john_king@web.de).  All rights reserved.
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License, version 3 as published by the
# Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

ARG PROJECT_NAME
ARG PROJECT_VERSION
ARG PROJECT_URL

ARG GCC_IMAGE="registry.gitlab.com/paul_john_king/docker.gcc:0.0.1_950201c"
ARG GLIBC_IMAGE="registry.gitlab.com/paul_john_king/docker.glibc:0.0.4_d76d5b0"
ARG LINUX_HEADERS_IMAGE="registry.gitlab.com/paul_john_king/docker.linux-headers:0.0.1_b13678d"
ARG WORKBENCH_IMAGE="registry.gitlab.com/paul_john_king/docker.ubuntu_workbench:0.0.18_7efec42"

ARG WORK_DIR="/work"
ARG TARGETS_DIR="${WORK_DIR}/targets"

FROM "${GCC_IMAGE}" AS GCC_IMAGE
FROM "${GLIBC_IMAGE}" AS GLIBC_IMAGE
FROM "${LINUX_HEADERS_IMAGE}" AS LINUX_HEADERS_IMAGE
FROM "${WORKBENCH_IMAGE}" AS WORKBENCH_IMAGE

	ARG WORK_DIR
	ARG TARGETS_DIR

	ARG IMAGES_DIR="${WORK_DIR}/images"
	ARG GCC_DIR="${IMAGES_DIR}/gcc"
	ARG GLIBC_DIR="${IMAGES_DIR}/glibc"
	ARG LINUX_HEADERS_DIR="${IMAGES_DIR}/linux_headers"
	ARG SOURCES_DIR="${WORK_DIR}/sources"

	ARG SOURCE_FILE="${SOURCES_DIR}/echo.c"
	ARG TARGET_FILE="${TARGETS_DIR}/echo"

	COPY --from="GCC_IMAGE" "/" "${GCC_DIR}"
	COPY --from="GLIBC_IMAGE" "/" "${GLIBC_DIR}"
	COPY --from="LINUX_HEADERS_IMAGE" "/" "${LINUX_HEADERS_DIR}"

	COPY "echo.c" "${SOURCE_FILE}"

	RUN \
		set -e; \
		set -u; \
		mkdir -p \
			"${SOURCES_DIR}" \
			"${TARGETS_DIR}"; \
		"${GCC_DIR}/bin/gcc" \
			"-static" \
			"-nostdinc" \
			"-I${GCC_DIR}/lib/gcc/x86_64-pc-linux-gnu/9.1.0/include" \
			"-I${GLIBC_DIR}/include" \
			"-I${LINUX_HEADERS_DIR}/include" \
			"${SOURCE_FILE}" \
			-o "${TARGET_FILE}"; \
		return;

FROM "scratch"

	ARG PROJECT_NAME
	ARG PROJECT_VERSION
	ARG PROJECT_URL

	ARG GCC_IMAGE
	ARG GLIBC_IMAGE
	ARG LINUX_HEADERS_IMAGE
	ARG WORKBENCH_IMAGE

	ARG TARGETS_DIR

	LABEL \
		project.name="${PROJECT_NAME}" \
		project.version="${PROJECT_VERSION}" \
		project.url="${PROJECT_URL}" \
		gcc_image="${GCC_IMAGE}" \
		glibc_image="${GLIBC_IMAGE}" \
		linux_headers_image="${LINUX_HEADERS_IMAGE}" \
		workbench_image="${WORKBENCH_IMAGE}"

	COPY --from="WORKBENCH_IMAGE" "${TARGETS_DIR}" "/"

	ENTRYPOINT ["/echo"]
