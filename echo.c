#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
	char* line ;
	size_t size ;
	ssize_t count ;

	line = NULL ;
	size = 0 ;

	while(1) {
		count = getline(&line, &size, stdin) ;
		if (count >= 0) {
			printf("%s", line) ;
		} else {
			free(line) ;
			return 0 ;
		}
	}
}
